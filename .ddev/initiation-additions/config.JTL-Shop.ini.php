<?php

define('PFAD_ROOT', '/var/www/html/web/');
define('DB_NAME', 'db');
define('DB_USER', 'db');
define('DB_PASS', 'db');

define('BLOWFISH_KEY', '898a6d439aef7f0c28ad5874ab9cd6');

define('EVO_COMPATIBILITY', false);

//enables printing of warnings/infos/errors for the shop frontend
define('SHOP_LOG_LEVEL', E_ALL);

//enables printing of warnings/infos/errors for the dbeS sync
define('SYNC_LOG_LEVEL', E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);

//enables printing of warnings/infos/errors for the admin backend
define('ADMIN_LOG_LEVEL', E_ALL);

//enables printing of warnings/infos/errors for the smarty templates
// @todo This is currently commented out, as it throws errors:
// define('SMARTY_LOG_LEVEL', E_ALL);

// explicitly turn off errors, as JTL just has too many deprecations:
ini_set('display_errors', 0);
// error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Set URL_SHOP URL and DB_HOST:
define('URL_SHOP', 'https://ddev-jtl-shop-5.ddev.site');
define('DB_HOST', 'ddev-ddev-jtl-shop-5-db');
