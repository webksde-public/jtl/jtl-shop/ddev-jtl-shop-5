#!/usr/bin/env bash

## Description: Startup A Drupal DDEV Environment, using DROWL Best Practices
## Usage: drowl-init
## Example: "drowl-init"

# exit when any command fails
set -e
# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG

# Define initiation constants:
PHP_VERSION=8.3
COMPOSER_VERSION="stable"
PROJECT_TYPE="php"
DB_NAME=ddev-"$DDEV_SITENAME"-db

# Clone JTL Store in web:
mkdir -p web

bool=1
while [ $bool -eq 1 ]; do
  read -p "Please put your unzipped jtl core files in the 'web' directory of the project and type 'yes' to continue..." answer
      case ${answer:0:1} in
        y|Y|yes|Yes|YES )
          echo "Great! continuing the setup..."
          # Use composer update -W instead of install here for existing projects to run the expected hooks:
          bool=0
        ;;
        * )
          echo "I don't understand, please type y,Y,yes,Yes or YES to continue"
        ;;
      esac
done

# Delete JTL PHPCS files if they exist:
if [ -f "./web/phpcs.xml" ]; then
  rm ./web/phpcs.xml;
fi
if [ -f "./web/phpcs-gitlab.xml" ]; then
  rm ./web/phpcs-gitlab.xml;
fi

# Create the config.yaml:
ddev config --composer-version="${COMPOSER_VERSION}" --php-version="${PHP_VERSION}" --docroot="web" --create-docroot --webserver-type="apache-fpm" --project-type="${PROJECT_TYPE}" --disable-settings-management --auto

# Starting Drupal DDEV Container
ddev start

# Add DEV dependencies (but no modules due to their database relationship):
ddev composer require --dev phpunit/phpunit phpspec/prophecy-phpunit phpstan/phpstan phpstan/phpstan-deprecation-rules squizlabs/php_codesniffer kint-php/kint

# Initialize development environment tools:
ddev exec chmod +x vendor/bin/phpcs
ddev exec chmod +x vendor/bin/phpcbf

echo "Acquiring initiation addition files..."

# Get the JTL-Shop config:
cp .ddev/initiation-additions/config.JTL-Shop.ini.php web/includes
# Set dynamic JTL-Shop db and shop parameters:
echo "define('URL_SHOP', '${DDEV_PRIMARY_URL}');" >> ./web/includes/config.JTL-Shop.ini.php
echo "define('DB_HOST','${DB_NAME}');" >> ./web/includes/config.JTL-Shop.ini.php

# Get VSCode Settings:
cp -r .ddev/initiation-additions/.vscode/ .

# Get PHPUnit.xml:
cp .ddev/initiation-additions/phpunit.xml web/phpunit

# Get the phpstan.neon:
cp .ddev/initiation-additions/phpstan.neon .

# Get esLint files:
cp .ddev/initiation-additions/.eslintrc.json ./web
cp .ddev/initiation-additions/.eslintignore ./web

# Get jsconfig.json:
cp .ddev/initiation-additions/jsconfig.json .

# Get packages for eslint:
echo 'Requiring ESLint npm packages...'
ddev npm init -y > /dev/null
ddev npm install --save-dev eslint@^8.57.0 eslint-plugin-import@^2.29.1
ddev npm install --save-dev eslint-config-airbnb-base@^15.0.0 prettier@^3.3.3 eslint-config-prettier@^9.1.0 eslint-plugin-prettier@^5.2.1
ddev npm install --save-dev eslint-plugin-yml@^1.14.0

# Get Backup file and import the file:
cp -r .ddev/initiation-additions/db-backup ./web
echo "Importing template database..."
ddev import-db --file=./web/db-backup/db.sql.gz

# Install the DDEV "PhpMyAdmin" and Ioncube plugins:
ddev get ddev/ddev-phpmyadmin
ddev get oblakstudio/ddev-ioncube

# Restart the containers for the plugins to get enabled:
ddev restart

# Get the Plugin Bootstrapper and JTL Debug plugins, for further commands and debugging purposes:
cd ./web/plugins
git clone https://gitlab.com/jtl-software/jtl-shop/plugins/jtl_plugin_bootstrapper.git
git clone https://gitlab.com/jtl-software/jtl-shop/plugins/jtl_debug.git
cd ../..

# Remove JTL install folder:
rm -r ./web/install

# Remove .git and .gitignore file:
echo "Removing .git folder and .gitignore file"
rm -r ./.git ./.gitignore -f

# Give all Project informations:
ddev describe

# Helper Messages
echo "Use 'ddev code' to attach and use VSCode with required extensions."
echo "Use 'ddev jtl-cli [command]' to execute the jtl shop cli."
echo "Use 'ddev phpunit path/to/tests' to Test Classes using PHPUnit."
echo "Use 'ddev phpcbf path/to/execute' format your Code using PSR-12 standards."
echo "Use 'ddev jtl-translate path/to/plugin' to create a 'base.po' file containing all translatable strings."

echo "Please manually enable the 'jtl_debug' plugin for debugging purposes."
echo "NOTE, if you want to use the jtl-cli 'create-plugin' command, you need to enable the 'jtl_plugin_bootstrapper' module!!"
