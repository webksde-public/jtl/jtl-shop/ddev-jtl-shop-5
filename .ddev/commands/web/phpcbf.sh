#!/bin/bash

## HostWorkingDir: true
## Description: Use phpcbf on a Folder for fixing Code using PSR-12 standards
## Usage: phpcbf [path]
## Example: "ddev phpcbf web/modules/contrib/devel"

if [ $# == 0 ]
then
  /var/www/html/vendor/bin/phpcbf --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml,phtml -p --standard=PSR12 "$PWD"
else
  /var/www/html/vendor/bin/phpcbf --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml,phtml -p --standard=PSR12 "$*"
fi
