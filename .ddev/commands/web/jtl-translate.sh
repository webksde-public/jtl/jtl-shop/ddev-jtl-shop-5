#!/bin/bash

## Description: Use jtl-translate to find all translatable "__('translate')" strings
## and put them inside a single translation file.
## Usage: jtl-translate [path]
## Example: "ddev jtl-translate /var/www/html/web/plugins/my_plugin"

# Check for required arguments
if [ $# -ne 1 ]; then
  echo -e $'\e\n[33mUsage: ddev jtl-translate <plugin-directory>\n\e[0m'
  exit 1
fi

# Store the input directory
input_dir=$1

if ! [ -f "$input_dir/info.xml" ]; then
  echo -e $"\e\n[33mIt doesn't look like you specified a plugin directory, since I can not find an 'info.xml'.\n\e[0m"
  exit 1
fi

# Define the output file location:
output_file="$input_dir/locale/en-GB/base.po"

# Check if base.po is already present and abort if that is the case:
if [ -f "$output_file" ]; then
  echo -e $"\e\n[33mThe '$output_file' file already exists. Please manually delete it first, before running this script.\n\e[0m"
  exit 1
fi

# Create the output file:
touch "$output_file"

# Function to check if string exists in file
string_exists() {
  grep -qF "$1" "$2"
}

# Find all occurences off "__("mystring")" inside all tpl and php files recursively and extract them:
find "$input_dir" -type f \( -name "*.tpl" -o -name "*.php" \) -exec grep -oP "__\(\K[\"']([^\"']+)[\"']\)" {} \; | while read -r line
do
{
  # Check if string already exists in the output file:
  # Note, the matched string will be in the form of "translatable_string") or 'another_translatable_string').
  # .po files are only valid with double quotes, so we remove the quote from the front and the quote from the end of
  # the string + the bracket.
  if ! string_exists "msgid \"${line:1: -2}\"" "$output_file"; then
    {
      echo "msgid \"${line:1: -2}\"";
      echo "msgstr \"${line:1: -2}\"";
      echo;
    } >> "$output_file";
  fi
}
done

# Extract strings between <Name> and <Description> tags in XML files
xml_files=$(find "$input_dir" -type f -name "info.xml")
for file in $xml_files; do
  names=$(grep -oP '(?<=<Name>).*?(?=<\/Name>)' "$file")
  descriptions=$(grep -oP '(?<=<Description>).*?(?=<\/Description>)' "$file")
  all_strings="$names"$'\n'"$descriptions"
  while IFS= read -r string; do
    if ! string_exists "msgid \"$string\"" "$output_file"; then
      {
        echo "msgid \"$string\""
        echo "msgstr \"$string\""
        echo
      } >> "$output_file"
    fi
  done <<< "$all_strings"
done

echo -e $"\e\n[32mTranslation strings extracted into your project directory: ${output_file}\n\e[0m"
echo -e $'\e\n[33mNote, that the translatable strings inside the info.xml need to get added manually!\n\e[0m'
echo -e $"\e\n[36mRun 'find . -name \*.po -execdir msgfmt base.po -o base.mo \;' INSIDE the projects folder, to convert all projects .po files to .mo files\n\e[0m"
