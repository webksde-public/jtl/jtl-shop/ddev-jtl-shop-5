#!/bin/bash

## HostWorkingDir: true
## Description: Use phpcs on a Folder for checking PSR-12 Coding standards
## Usage: phpcs [path]
## Example: "ddev phpcs web/modules/contrib/devel"

if [ $# == 0 ]
then
  phpcs -p --standard=PSR12 $PWD
else
  phpcs -p --standard=PSR12 $*
fi
