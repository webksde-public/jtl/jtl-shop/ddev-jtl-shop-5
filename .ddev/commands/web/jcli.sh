#!/bin/bash

## Description: Use jtl-cli [command] to exectue a jtl shop cli command, see all commands here (jtl_plugin_bootstrapper is enabled by default): https://docs.jtl-shop.de/de/latest/shop_administration/shop_cli.html
## Usage: jtl-cli [command]
## Example: "ddev jtl-cli cache:clear"

cd ./web || exit
php cli "$@"
