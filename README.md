# DDEV JTL Shop 5

DDEV Environment using JTL Shop 5 and attached VS-Code Container

## Quick-Start
~~~
git clone https://gitlab.com/webksde-public/jtl/jtl-shop/ddev-jtl-shop-5.git && cd ddev-jtl-shop-5/ && ddev drowl-init
~~~

## Prerequisites
  1. Up to date Version of [DDEV](https://ddev.com/), [Docker Desktop](https://www.docker.com/products/docker-desktop/) / [Docker CE](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository) and Chrome/Firefox
  2. VSCode installed on your machine locally
  3. The [Remote Development Extension for VSCode (extension name: ms-vscode-remote.vscode-remote-extensionpack)}(https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)

## Getting started
 1. Clone the the repo to a new empty project directory: `git clone git@gitlab.com:webksde-public/jtl/jtl-shop/ddev-jtl-shop-5.git`
 2. Start / Restart Docker and make sure it is updated to the current version
 3. Change into the directory: `cd ddev-jtl-shop-5/`
 4. Use `ddev drowl-init` to start up the environment.
 5. You are ready to go! Use `ddev describe` to check the status & URL of your Project and `ddev code` to run your prepared VSCode IDE!

## Tooling
 - Use `ddev code` to attach VSCode to your running Container.
 - Use `ddev jtl-cli [command]` to execute the jtl shop cli.
 - Use `ddev phpunit path/to/tests` to Test Classes using PHPUnit.
 - Use `ddev phpcs path/to/sniff` to check your Code using PSR-12 Standards.
 - Use `ddev phpcbf path/to/execute` format your Code using PSR-12 standards
 - Use `ddev phpstan path/to/execute` to look for deprecated and 'dirty' code.
 - Use `ddev jtl-translate path/to/plugin` to create a 'base.po' file containing all translatable strings
